import React from 'react'
import VideoPlayerContainer from './containers/VideoPlayerContainer'

export default function (props) {
  return (
    <div className='container'>
      <VideoPlayerContainer />
    </div>
  )
}
