import { connect } from 'react-redux'
import { setCaptionsOn, setCaptions, setScroll } from '../store/actions'
import { getCaptions } from '../services/caption-service'
import Buttons from '../components/Buttons.jsx'

function mapStatetoProps (state) {
  return {
    player: state.videoPlayer.get('player'),
    captions: state.captions,
    captionsOn: state.videoPlayer.get('captionsOn'),
    scroll: state.videoPlayer.get('scroll')
  }
}

function mapDispatchToProps (dispatch, ownProps) {
  const getCaptionsAsync = () => {
    return (dispatch) => {
      getCaptions((captions) => {
        dispatch(setCaptions(captions))
      })
    }
  }
  return {
    toggleCaptions (player, captions, bool) {
      dispatch(setCaptionsOn(bool))
      if (bool === true && !captions.size) {
        dispatch(getCaptionsAsync())
      }
    },
    toggleScroll (bool) {
      dispatch(setScroll(bool))
    }
  }
}

export default connect(
  mapStatetoProps,
  mapDispatchToProps
)(Buttons)
