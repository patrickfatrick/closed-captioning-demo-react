import React, { PropTypes } from 'react'
import PureRenderMixin from 'react-addons-pure-render-mixin'
import ImmutablePropTypes from 'react-immutable-proptypes'
import m from './m'

export default React.createClass({
  mixins: [PureRenderMixin],
  styles: {
    buttonContainer: {
      marginTop: '1rem',
      textAlign: 'center'
    },
    button: {
      marginRight: '0.3rem',
      marginLeft: '0.3rem'
    }
  },
  propTypes: {
    captionsOn: PropTypes.bool.isRequired,
    captions: ImmutablePropTypes.listOf(
      ImmutablePropTypes.contains({
        start: PropTypes.number.isRequired,
        end: PropTypes.number.isRequired,
        caption: PropTypes.string.isRequired,
        active: PropTypes.bool.isRequired
      }).isRequired
    ).isRequired,
    scroll: PropTypes.bool.isRequired
  },
  render () {
    return (
      <div id='button-container' style={m(this.styles.buttonContainer)}>
        <button
          id='closed-captions-button'
          title='Closed Captions'
          style={m(this.styles.button)}
          onClick={() => this.props.toggleCaptions(this.props.player, this.props.captions, !this.props.captionsOn)}>
          Turn {(this.props.captionsOn) ? 'off' : 'on'} Closed Captioning
        </button>
        {(this.props.captionsOn) &&
          <button
            id='closed-captions-scroll-button'
            title='Auto-Scroll'
            style={m(this.styles.button)}
            onClick={() => this.props.toggleScroll(!this.props.scroll)}>
            Turn {(this.props.scroll) ? 'off' : 'on' } Auto-Scroll
          </button>
        }
      </div>
    )
  }
})
